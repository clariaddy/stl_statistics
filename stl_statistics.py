#!/usr/bin/env python3
import os
import argparse
import warnings
warnings.filterwarnings('ignore')  # disable annoying numpy deprecation messages

try:
    import numpy as np
    import stl
except ImportError:
    msg = ('\nFATAL ERROR: Could not find numpy or numpy-stl python modules.'
           '\nUse \'pip3 install --upgrade -r requirements.txt\' to install them.'
           '\n')
    print(msg)
    raise

units_to_factor = {
    'm': 1e-0,
    'mm': 1e-3,
    'um': 1e-6,
    'nm': 1e-9,
    'pm': 1e-12,
}

def compute_surface(mesh):
    surface = 0.0
    for triangle in mesh.vectors:
        X0,X1,X2 = triangle
        u, v = X1-X0, X2-X0
        w = np.cross(u, v)
        surface += np.sqrt(np.dot(w, w)) / 2.0
    return surface

def compute_volume(mesh):
    volume = 0.0
    for triangle in mesh.vectors:
        X0,X1,X2 = triangle
        volume += np.dot(X0, np.cross(X1, X2)) / 6.0
    return volume

def compute_surface_fast(mesh):
    Vt = mesh.vectors
    U = Vt[:,1]-Vt[:,0]
    V = Vt[:,2]-Vt[:,0]
    W = np.cross(U,V)
    S = np.sum(np.sqrt(np.sum(W*W, axis=1))) / 2.0
    return S

def compute_volume_fast(mesh):
    Vt = mesh.vectors
    V = np.sum(Vt[:,0]*np.cross(Vt[:,1], Vt[:,2])) / 6.0
    return V

def compute_mesh_statistics(args):
    mesh = args.mesh
    shape = mesh.vectors.shape
    pts = mesh.vectors.reshape(shape[0] * shape[1], shape[2])
    
    # Correctly use args.factor
    scaling_factor = args.factor if args.factor is not None else units_to_factor[args.units]

    pmin = np.min(pts, axis=0) * scaling_factor
    pmax = np.max(pts, axis=0) * scaling_factor
    L = pmax - pmin

    S = compute_surface_fast(mesh) * (scaling_factor ** 2)
    V = compute_volume_fast(mesh) * (scaling_factor ** 3)

    print('''
    Mesh '{}' statistics (units={}, factor={}):
     pmin: {}
     pmax: {}
     L:    {}
     S:    {} {u}^2
     V:    {} {u}^3
     S/V:  {} {u}^-1
    '''.format(args.file_name, args.units, scaling_factor,
               pmin, pmax, L, S, V, S/V, u=args.units))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Compute simple STL mesh statistics.')
    parser.add_argument('file_name', type=str, help='Path to the stl mesh.')
    parser.add_argument('-u', '--units', type=str, default='m', help='Predefined units scaling. Available: m, mm, um, nm, pm.')
    parser.add_argument('-s', '--factor', type=float, default=None, help='Custom scaling factor.')  # Changed from -f to -s to match your usage

    args = parser.parse_args()
    
    if not os.path.exists(args.file_name):
        parser.error('File \'{}\' does not exist.'.format(args.file_name))

    try:
        args.mesh = stl.mesh.Mesh.from_file(args.file_name)
    except:
        parser.error('Could not load STL mesh file \'{}\'.'.format(args.file_name))

    if args.factor is None:  # Corrected attribute reference
        if args.units in units_to_factor:
            args.factor = units_to_factor[args.units]
        else:
            try:
                args.factor = float(args.units)
                args.units = 'custom'
            except ValueError:
                parser.error('Could not convert custom factor \'{}\' to float.'.format(args.units))

    compute_mesh_statistics(args)