# Stl statistics

Compute the surface and the volume of a watertight stl meshes.

Requires python3, numpy and numpy-stl modules.

For usage see `python3 stl_statitics.py --help`.
